<?php
/**
 * {{vendor}U}_{{module}U}
 */

/**
 * Class {{vendor}U}_{{module}U}_Helper_Data
 * @author Lorenzo Calamandrei <nexcal.dev@gmail.com>
 * @version 0.1.0
 * @package NexCal_Logger
 */
class {{vendor}U}_{{module}U}_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * @const LOG_FILE where log of {{vendor}U}_{{module}U}
	 */
	const LOG_FILE = '{{vendor}}_{{module}}.log';

	/**
	 * @const LOG LEVEL.
	 */
	const LOG_ERROR = 1,
		LOG_CRITICAL = 2,
		LOG_DEBUG = 5,
		LOG_INFO = 7;

	/**
	 * @const LOG_SEPARATOR
	 */
	const LOG_SEPARATOR = '------------------';

	/**
	 * getConfig
	 * @param string $code
	 * @return mixed
	 */
	public function getConfig($code)
	{
		return Mage::getStoreConfig('{{vendor}}_{{module}}/' . $code);
	}

	/**
	 * isEnabled
	 * @return mixed
	 */
	public function isEnabled()
	{
		return $this->getConfig('settings/enabled');
	}

	/**
	 * log
	 * @param $obj
	 * @param int $importance
	 * @return bool
	 */
	public function log($obj, $importance, $level = 0)
	{
		if ($importance > 1
			&& !$this->isEnabled()
			&& !$this->getConfig('settings/log_enabled')
		) {
			return false;
		}

		$indent = '';
		for ($i = 0; $i < $level; $i++) {
			$indent .= "\t";
		}

		if (is_string($obj)) {
			Mage::log($indent . $obj, $importance, self::LOG_FILE, true);
		} else {
			Mage::log($obj, $importance, self::LOG_FILE);
		}

		return true;
	}
}
