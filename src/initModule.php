<?php

const DS = '/';

function replace($string, $options, $upperVendor, $upperModule)
{
	$moduleDeclaration = str_replace('{{vendor}}', $options['vendor'], $string);
	$moduleDeclaration = str_replace('{{module}}', $options['module'], $moduleDeclaration);
	$moduleDeclaration = str_replace('{{vendor}U}', $upperVendor, $moduleDeclaration);
	$moduleDeclaration = str_replace('{{module}U}', $upperModule, $moduleDeclaration);
	$moduleDeclaration = str_replace('{{codePool}}', $options['codePool'], $moduleDeclaration);
	return $moduleDeclaration;
}

$options = getopt(null, [
	'codePool:',
	'vendor:',
	'module:',
	'output:',
]);

if (empty($options)) {
	throw new Exception('Not all parameters entered!');
}

if (count($options) !== 4) {
	throw new Exception('Not all parameters entered!');
}

foreach ($options as $option) {
	if ($option === false) {
		throw new Exception('Not all parameters entered!');
	}
}

$upperVendor = ucwords($options['vendor']);
$upperModule = ucwords($options['module']);

echo "Folder construction...\n";

echo "Creating base structure:\n";

$options['output'] = $options['output'] . DS . "{$options['vendor']}_{$options['module']}";

if (!mkdir($concurrentDirectory = $options['output']) && !is_dir($concurrentDirectory)) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

// module folder
if (
	!mkdir(
		$concurrentDirectory = ($options['output']
			. DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}"),
		0777,
		true
	)
	&& !is_dir($concurrentDirectory)
) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

// etc folder
if (
	!mkdir(
		$concurrentDirectory = ($options['output']
			. DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/etc")
	)
	&& !is_dir($concurrentDirectory)
) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

// Helper folder
if (
	!mkdir(
		$concurrentDirectory = ($options['output']
			. DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/Helper")
	)
	&& !is_dir($concurrentDirectory)
) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

// etc/modules folder
if (
	!mkdir(
		$concurrentDirectory = ($options['output']
			. DS . 'app/etc/modules'),
		0777,
		true
	)
	&& !is_dir($concurrentDirectory)
) {
	throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
}

echo "Folder structure created.\n";
echo "Module preparation init.\n";

// XML
$moduleDeclaration = file_get_contents(__DIR__ . '/base/xml/etc/nomeModule.xml');
$moduleDeclaration = replace($moduleDeclaration, $options, $upperVendor, $upperModule);
file_put_contents(
	$options['output'] . DS . "app/etc/modules/{$upperVendor}_{$upperModule}.xml",
	$moduleDeclaration
);

$configXML = file_get_contents(__DIR__ . '/base/xml/module/etc/config.xml');
$configXML = replace($configXML, $options, $upperVendor, $upperModule);
file_put_contents(
	$options['output'] . DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/etc/config.xml",
	$configXML
);

$systemXML = file_get_contents(__DIR__ . '/base/xml/module/etc/system.xml');
$systemXML = replace($systemXML, $options, $upperVendor, $upperModule);
file_put_contents(
	$options['output'] . DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/etc/system.xml",
	$systemXML
);

$adminhtmlXML = file_get_contents(__DIR__ . '/base/xml/module/etc/adminhtml.xml');
$adminhtmlXML = replace($adminhtmlXML, $options, $upperVendor, $upperModule);
file_put_contents(
	$options['output'] . DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/etc/adminhtml.xml",
	$adminhtmlXML
);

// PHP
$DataPHP = file_get_contents(__DIR__ . '/base/php/Helper/Data.php');
$DataPHP = replace($DataPHP, $options, $upperVendor, $upperModule);
file_put_contents(
	$options['output'] . DS . "app/code/{$options['codePool']}/{$upperVendor}/{$upperModule}/Helper/Data.php",
	$DataPHP
);

echo "Finish! Enjoy your new module! :D\n";