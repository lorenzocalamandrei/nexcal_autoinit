# Nexcal AutoInit
---
## How to use
```php
php src/initModule.php --codePool community --vendor Nexcal --module Test --output ~/Desktop
```

This command executed in the root folder of the project will generate a basic module for Magento 1 with the minimum settings including an activation / deactivation of the module. 

---

* developed by Lorenzo Calamandrei <nexcal.dev@gmail.com>